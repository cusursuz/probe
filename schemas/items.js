var mongoose = require ('mongoose')
, Schema = mongoose.Schema;

var itemSchema = Schema({
	id_man: { type: Schema.Types.ObjectId, ref: 'manufacturer' },
	model: String,
	price: Number,
	wiki: String,
	image: String
  });

module.exports = mongoose.model('item', itemSchema);