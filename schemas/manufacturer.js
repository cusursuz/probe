var mongoose = require ('mongoose')
, Schema = mongoose.Schema;

var manufacturerSchema = Schema({
	//_id: { type: Schema.Types.ObjectId, ref: 'item' },
	manufacturer:  String,
	logo: String
  });

module.exports = mongoose.model('manufacturer', manufacturerSchema);