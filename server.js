var DEFAULT_PORT = 3000;
var express = require('express');
var app = express();
var admin = express(); // admin-ka
var mongojs = require('mongojs');
var db = mongojs('cars', ['manufacturers', 'items']);
var bodyParser = require('body-parser');
var mongoose = require ('mongoose');
var db_connection = require('./db');
var dataManufacturer = require('./schemas/manufacturer');
var dataItem = require('./schemas/items');
const fs = require('fs');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

/*var route = require('./route');
app.use('/', route);
*/

app.use(express.static(__dirname + '/public')); //подключаем папку public
app.use(bodyParser.json());
admin.use(multipart({
		uploadDir: "public/upload",
	}));


/* ++++++++++++++++++++++++ Manufacturer block*/
app.post('/man',function (req, res){ // Запись данных в БД
	var manufacturer = req.body.manufacturer;
	var logo = req.body.logo;
	var newManufacturer = new dataManufacturer({ manufacturer: manufacturer, logo: logo})
	
	newManufacturer.save(function (err, newUser) {
	     if (err){
	         console.log("Something goes wrong with manufacturer " + newManufacturer.manufacturer);
	     }else{
	         console.log("Totul a mers bine si este inscris in db " + newManufacturer.manufacturer);
	         res.send("OK");
	     }
	 });
});

app.delete('/man/:id/:logo', function (req, res) {
	console.log(req.params.id);
	db.items.findOne({id_man:mongojs.ObjectId(req.params.id)}, function(err, result) {
	    if (err) { /* handle err */ }
			console.error(err);
	    if (result) {
	        // we have a result
			console.log("Удаление не возможно");
			res.status(403).send();
	    } else {
			var id = req.params.id;
				var logo = req.params.logo;
				db.manufacturers.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
			    	res.json(doc);
					fs.unlink('public/upload/'+logo, function(err) {
						  if (err) {
						  	console.log(process.cwd());
				       return console.error(err);
				   }
				   console.log("File deleted successfully!");
						});
			});
	    }
	});
});

app.get('/man/:id', function (req, res) { //put data into input

  var id = req.params.id;
  console.log(id);
  db.manufacturers.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});
app.put('/man/:id', function (req, res) { // update document
	var id = req.params.id;
  	console.log(req.body);

	db.manufacturers.findAndModify({
		query: {_id: mongojs.ObjectId(id)},
    	update: {$set: {manufacturer: req.body.manufacturer, logo: req.body.logo}},
    	new: true}, function (err, doc) {
			res.json(doc);
	    	}
	    );
});
/* ++++++++++++++++++++ end Manufacturer block*/

app.get('/cars', function (req, res){
//	console.log("Я получил get1");
	dataItem.find().populate({
	    path: 'id_man'
	  , options: { sort: { manufacturer: -1 }}
	}).exec(function (err, dataItem) {
		res.json(dataItem);
	})
});

app.get('/manufacturer', function (req, res){
// Получаем данные из БД
	db.manufacturers.find(function(err, docs){ 
			//console.log(docs);
			res.json(docs);
		});	
});


app.post('/cars',function (req, res){ // Запись данных в БД
	//console.log(req.body);
	var man_id = req.body.id_man._id;
	var model = req.body.model;
	var price = req.body.price;
	var image = req.body.image;
	var wiki = req.body.wiki;
	var newItem = new dataItem({ id_man: man_id, model: model, price: price, wiki: wiki, image: image})
	 //console.log(newItem);
	 newItem.save(function (err, newUser) {
	     if (err){
	         console.log("Something goes wrong with item " + newItem.model);
	     }else{
	         console.log("Totul a mers bine si este inscris in db " + newItem.model);
	         res.send("OK");
	     }
	 });
});

// Удаляем запись из БД
app.delete('/cars/:id/:image', function (req, res) {
	/*console.log(req.params.id);
	console.log(req.params.image);*/
	var id = req.params.id;
	var image = req.params.image;
	db.items.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    	res.json(doc);
		fs.unlink('public/upload/'+image, function(err) {
			  if (err) {
			  	console.log(process.cwd());
	       return console.error(err);
	   }
	   console.log("File deleted successfully!");
			});
	});
});

app.get('/cars/:id', function (req, res) { //put data into input
	var id = req.params.id;
	dataItem
	.findOne({ _id: id })
	.populate('id_man')
	.exec(function (err, dataItem) {
		if (err) return handleError(err);
		console.log('The manufacturer is - %s', dataItem.id_man.manufacturer);
		res.json(dataItem);
	});
});

app.put('/cars/:id', function (req, res) { // update document
	var id = req.params.id;
  	console.log(req.body);

	db.items.findAndModify({
		query: {_id: mongojs.ObjectId(id)},
    	update: {$set: {id_man: req.body.id_man._id, model: req.body.model, image: req.body.image, price: req.body.price, wiki: req.body.wiki}},
    	new: true}, function (err, doc) {
			res.json(doc);
	    	}
	    );
});
app.use('/admin', admin); // mount the sub app
admin.get('/', function (req, res) {
	console.log(admin.mountpath); // /admin
  //res.send('Admin Homepage');
	db.cars.find(function(err, docs){ 
		console.log(docs);
		res.json(docs);
	});	
});

admin.post('/upload', multipartMiddleware, function(req, resp) { // file upload
  //console.log(req.body, req.files);
  resp.end(req.files.file.path);
});

app.listen(DEFAULT_PORT);
console.log("server is running on port "+ DEFAULT_PORT);