var carApp = angular.module('carApp', ['ngFileUpload']);
carApp.controller('CarCtrl', ['$scope', '$http', 'Upload', '$timeout', function($scope, $http, Upload, $timeout) {
    //console.log("Тебя приветствует контроллер");

// - fileupload
$scope.uploadPic = function(file, scope) {
	console.log(scope);
    file.upload = Upload.upload({
      //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
      url: 'upload',
      data: {file: file, username: $scope.username},
    });
	
    file.upload.then(function (response) {
    	console.log(response.data);
    	var str = response.data;
	    var res = str.substring(14);
    	$scope.car.image = res;    	
    	$scope.man.logo = res;
    	$timeout(function () {
        file.result = response.data;
    	});
    }, function (response) {

      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
	$scope.imageshowModal = !$scope.imageshowModal;
	//$scope.car.image = response.data;    	
}
   // - fileupload

	$scope.imageshowModal = false;
    $scope.imagetoggleModal = function(){
        $scope.imageshowModal = !$scope.imageshowModal;
    };
	$scope.showModal = false;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };


	var refresh = function(){ // обновление браузера и данных из бд
		$http.get('/cars').success(function(response){
			$scope.cars = response;
			$scope.car = {
				id_man : {}
			};
			console.log(response);
		});

		$http.get('/manufacturer').success(function(response){
			$scope.manufacturer = response;
			$scope.man = {};
			console.log(response);
		});

	};

	refresh();

	$scope.addContact = function(){
			// выводим в консоль данные из input-ов
			// после нажатия кнопки с функцией addContact()
		console.log($scope.car);
		// получаем ответ от сервера после отправки данных
		$http.post('/cars', $scope.car).success(function(response){
			//console.log("Ответ от сервера"+response);
			console.log(response);
			refresh();
		});

	};

	$scope.remove = function(id,image){
		//выводим id в консоль
		console.log(id);
		$http.delete('/cars/' + id+'/'+image).success(function(response){
			refresh();
		});
		
	};

	$scope.edit = function(id){ //выводим данные в input из БД
		console.log(id);
		$http.get('/cars/' + id).success(function(response){
			$scope.car = response;
			console.log(response);
		});
	};

	$scope.deselect = function(){ // Очищаем input-ы
		$scope.car = {
			id_man : {}
		};
		$scope.man = {}
	};

	$scope.update = function(){ //Обновляем БД
		console.log($scope.car._id);
		$http.put('/cars/'+ $scope.car._id, $scope.car).success(function(response){
			refresh();
		});
	};


$scope.selectMan = function(manufacturer, id){
	console.log($scope.car);
	//console.log('work and id is %s', id);
	$scope.car.id_man.manufacturer = manufacturer;
	$scope.car.id_man._id = id;
	
	//console.log($scope.car.id_man);
	$scope.showModal = !$scope.showModal;
}


$scope.addContactMan = function(){
		console.log($scope.man);
		// получаем ответ от сервера после отправки данных
		$http.post('/man', $scope.man).success(function(response){
			//console.log("Ответ от сервера"+response);
			console.log(response);
			
		});
	refresh();
	};
$scope.removeMan = function(id,logo){
		//выводим id в консоль
		console.log(id);
		$http.delete('/man/' + id+'/'+logo).success(function(response){
			refresh();
		}).error(function(response){
			 alert('Удаление невозможно!');
		});
		
	};
$scope.editMan = function(id){ //выводим данные в input из БД
		console.log(id);
		$http.get('/man/' + id).success(function(response){
			$scope.man = response;
			console.log(response);
		});
	};
$scope.updateMan = function(){ //Обновляем БД
		console.log($scope.man._id);
		$http.put('/man/'+ $scope.man._id, $scope.man).success(function(response){
			refresh();
		});
	};
}]).directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  }).directive('imagemodal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });